CREATE TABLE public.ssh_attempts
(
    id       uuid                        DEFAULT gen_random_uuid() NOT NULL,
    ts       timestamp without time zone DEFAULT now()             NOT NULL,
    ip       character varying(255)                                NOT NULL,
    username character varying(255)                                NOT NULL,
    password character varying(255)                                NOT NULL
);

CREATE TABLE public.ssh_factories
(
    id      uuid DEFAULT gen_random_uuid() NOT NULL,
    factory character varying(255)         NOT NULL
);

ALTER TABLE ONLY public.ssh_factories
    ADD CONSTRAINT ssh_factories_factory_key UNIQUE (factory);

ALTER TABLE ONLY public.ssh_factories
    ADD CONSTRAINT ssh_factories_pkey PRIMARY KEY (id);
