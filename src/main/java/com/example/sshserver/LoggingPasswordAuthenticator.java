package com.example.sshserver;

import org.apache.sshd.server.auth.AsyncAuthException;
import org.apache.sshd.server.auth.password.PasswordAuthenticator;
import org.apache.sshd.server.auth.password.PasswordChangeRequiredException;
import org.apache.sshd.server.session.ServerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;

@Component
public class LoggingPasswordAuthenticator implements PasswordAuthenticator {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final JdbcTemplate jdbcTemplate;

	public LoggingPasswordAuthenticator(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public boolean authenticate(String username,
															String password,
															ServerSession session) throws PasswordChangeRequiredException, AsyncAuthException {
		InetSocketAddress socketAddress = (InetSocketAddress) session.getClientAddress();
		String ip = socketAddress.getAddress().getHostAddress();
		String[] factories = session.getCipherFactoriesNameList().split(",");
		logger.info("Login attempt from {} as {}/{} {}", ip, username, password, factories);
		for (String factory : factories) {
			jdbcTemplate.update("INSERT INTO ssh_factories (factory) VALUES (?) ON CONFLICT DO NOTHING", factory);
		}
		jdbcTemplate.update("INSERT INTO ssh_attempts (ip, username, password) VALUES (?, ?, ?)", ip, username, password);
		return false;
	}
}
