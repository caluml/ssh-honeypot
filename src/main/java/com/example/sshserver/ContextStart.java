package com.example.sshserver;

import org.apache.sshd.common.PropertyResolverUtils;
import org.apache.sshd.common.keyprovider.FileKeyPairProvider;
import org.apache.sshd.core.CoreModuleProperties;
import org.apache.sshd.server.SshServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

@Component
public class ContextStart implements ApplicationListener<ApplicationEvent> {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final JdbcTemplate jdbcTemplate;

	private SshServer sshd;

	public ContextStart(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public void onApplicationEvent(ApplicationEvent event) {
		logger.info("Application event: " + event);
		if (event instanceof ApplicationStartedEvent) {
			try {
				logger.info("Starting SSH server");
				sshd = SshServer.setUpDefaultServer();
				sshd.setPort(2222);
				sshd.setKeyPairProvider(new FileKeyPairProvider(ResourceUtils.getFile("classpath:sshserverkey.pem").toPath()));
				sshd.setPasswordAuthenticator(new LoggingPasswordAuthenticator(jdbcTemplate));

				sshd.setCommandFactory(null);
				sshd.setShellFactory(null);

				PropertyResolverUtils.updateProperty(sshd, CoreModuleProperties.SERVER_IDENTIFICATION.getName(), "OpenSSH_9.2p1 Debian-2");

				sshd.start();
			} catch (Exception e) {
				logger.error("Error starting SSH server", e);
			}
		} else if (event instanceof ContextClosedEvent) {
			try {
				logger.info("Stopping SSH server");
				sshd.stop();
			} catch (Exception e) {
				logger.error("Error stopping SSH server", e);
			}
		}
	}
}
