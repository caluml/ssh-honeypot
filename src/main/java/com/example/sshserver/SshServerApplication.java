package com.example.sshserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SshServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SshServerApplication.class, args);
	}

}
