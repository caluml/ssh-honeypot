## SSH server

Simple honeypot SSH server that records all usernames/passwords attempted to a database.<br>
Currently uses Postgres, but would be easy to amend

### Build
Fill in the correct DB values in `src/main/resources/application.properties`<br>
./mvnw clean package

### Deploy
Run the `src/main/resources/sql/schema.sql` file to generate the tables<br>
Upload the war to a tomcat server.

### Redirect
It runs on port 2222 (so it doesn't require root privileges).<br>
You can redirect port 22 traffic to it with:
```bash
iptables -t nat -I PREROUTING -p tcp --dport 22 -j DNAT --to-destination :2222
```
Make sure you aren't running your real SSH server on port 22 though.

### Some queries
#### Where are the queries coming from?
```sql
SELECT COUNT(ip), ip FROM ssh_attempts WHERE ts > NOW() - INTERVAL '1 DAY' GROUP BY ip ORDER BY COUNT(ip) DESC;
```
#### What are the most common passwords tried?
```sql
SELECT COUNT(password), password FROM ssh_attempts WHERE ts > NOW() - INTERVAL '1 DAY' GROUP BY password ORDER BY COUNT(password) DESC;
```